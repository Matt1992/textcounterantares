//
//  TextViewCounter.swift
//  TextViewCounter
//
//  Created by Mateusz Kopacz on 13/07/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//


import UIKit

public class TextViewCounter: UIView{
   
    /// Set TextView height
   public var textViewHeight = UIScreen.main.bounds.height
    
    /// Set number of characters
   public var maximumCharacterNumber = 300
    
   public let textView: UITextView = {
        let textView = UITextView()
        textView.isScrollEnabled = true
        textView.font = UIFont.systemFont(ofSize: 16)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.backgroundColor = .white
        textView.textColor = .secondaryLabel
        textView.textAlignment = .justified
    
        return textView
    }()
    
    /**
     Pleceholder for TextView.
     
     - Note: Placeholder is type o UILabel. You can change in the placeholder everything what is in UILabel
    */
   public let placeholder: UILabel = {
        let placeholderLabel = UILabel()
        placeholderLabel.text = NSLocalizedString("Write something", comment: "")
        placeholderLabel.font = UIFont.systemFont(ofSize: 16)
        placeholderLabel.sizeToFit()
        placeholderLabel.translatesAutoresizingMaskIntoConstraints = false
        placeholderLabel.textColor = UIColor.lightGray
        return placeholderLabel
    }()
    
    
    /**
     Character counter displays number o characters user types and maximum number of characters.

    */
   public let characktersCountLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .right
        return label
    }()
    

    
   /**
    Button for save changes

   */
   public var saveChnangesButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(NSLocalizedString("Save Changes", comment: ""), for: .normal)
        button.setTitleColor(.purple, for: .normal)
        return button
    }()
    
    
   /**
    Current Text Value displays text if there is currently saved text

   */
   public var currentTextValue: String? {
        didSet{
            guard let unwrappedAbout = currentTextValue else {return}
                if unwrappedAbout.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                    self.textView.text = unwrappedAbout
                    self.characktersCountLabel.text = "\(unwrappedAbout.count)/\(maximumCharacterNumber)"
                    self.placeholder.isHidden = !self.textView.text.isEmpty
                }
            }
    }
    
    
    /**
     Text counter displays currently characters number and maximum characters number

    */
   public var textCounter: String? {
        didSet{
            guard let unwrappedText = textCounter else {return}
            characktersCountLabel.text = "\(unwrappedText.count)/\(maximumCharacterNumber)"
        }
    }
    
    
    
   public override init(frame: CGRect) {
        super.init(frame: frame)
        initConstraints()
        textView.delegate = self
        characktersCountLabel.text = "0/\(maximumCharacterNumber)"
    }
    

    
    func initConstraints(){
        

        addSubview(textView)
        // Add placeholder to textview
        textView.addSubview(placeholder)
        placeholder.isHidden = !textView.text.isEmpty
        placeholder.frame.origin = CGPoint(x: 5, y: (textView.font?.pointSize)! / 2)
        addSubview(characktersCountLabel)
        addSubview(saveChnangesButton)
        
        
        // Set constraints for text view
        NSLayoutConstraint.activate([

            textView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 40),
            textView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            textView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            textView.heightAnchor.constraint(equalToConstant: textViewHeight/3),
        ])
        
        // Set constraints for text view placeholder
        NSLayoutConstraint.activate([
            placeholder.leadingAnchor.constraint(equalTo: textView.leadingAnchor, constant: 5),
            placeholder.trailingAnchor.constraint(equalTo: textView.trailingAnchor, constant: -5),
            placeholder.topAnchor.constraint(equalTo: textView.topAnchor, constant: (textView.font!.pointSize) / 2),
        
        ])
        
        // Set constraints for character count label
        NSLayoutConstraint.activate([
           characktersCountLabel.topAnchor.constraint(equalTo: textView.bottomAnchor, constant: -10),
           characktersCountLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
           characktersCountLabel.widthAnchor.constraint(equalToConstant: 80),
            
        ])
        
        // Set constraints for save changes button
        NSLayoutConstraint.activate([
            saveChnangesButton.topAnchor.constraint(equalTo: characktersCountLabel.bottomAnchor, constant: 40),
            saveChnangesButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            saveChnangesButton.widthAnchor.constraint(equalToConstant: 150),
            saveChnangesButton.heightAnchor.constraint(equalToConstant: 40),
            
        ])


            
        
    }
    
    
  public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



extension TextViewCounter: UITextViewDelegate{

    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            // get the current text, or use an empty string if that failed
        let currentText = textView.text ?? ""

        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }

        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)

        // make sure the result is under 16 characters
        return updatedText.count <= maximumCharacterNumber
    }
    

}
